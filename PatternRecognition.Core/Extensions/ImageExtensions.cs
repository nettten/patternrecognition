﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;

namespace PatternRecognition.Core.Extensions
{
    /// <summary>
    /// Методы расширения для класса Image OpenCV
    /// </summary>
    public static class ImageExtensions
    {
        /// <summary>
        /// Поиск внешних контуров объектов на полноцветном изображении
        /// </summary>
        /// <param name="source">Полноцветное изображение</param>
        /// <returns>Коллекция точек найденных контуров</returns>
        public static VectorOfVectorOfPoint FindContours(this Image<Bgr, byte> source)
        {
            VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint();

            using (Image<Gray, byte> grayImage = source.Convert<Gray, byte>())
            using (Image<Bgr, byte> denoiseImage = new Image<Bgr, byte>(source.Width, source.Height))
            {
                CvInvoke.FastNlMeansDenoising(grayImage, denoiseImage);

                using (Image<Gray, byte> binaryImage = new Image<Gray, byte>(grayImage.Width, grayImage.Height, new Gray(0)))
                {
                    CvInvoke.AdaptiveThreshold(denoiseImage, binaryImage, 255, AdaptiveThresholdType.GaussianC, ThresholdType.Binary, 3, 0);
                    CvInvoke.FindContours(binaryImage, contours, null, RetrType.External, ChainApproxMethod.ChainApproxNone);
                }
            }

            return contours;
        }
    }
}
