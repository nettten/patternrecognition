﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using AForge.Math;

namespace PatternRecognition.Core.Extensions
{
    /// <summary>
    /// Методы расширения для коллекции, состоящей из коллекции двумерных точек
    /// </summary>
    public static class PointsCollectionsExtensions
    {
        /// <summary>
        /// Перенос коллекции точек в начало координат
        /// </summary>
        /// <param name="pointsCollections">Коллекция точек</param>
        /// <returns>Коллекция точек, перенесенных в начало координат</returns>
        public static IEnumerable<IEnumerable<Point>> ReplaceToCoordinateStart(this IEnumerable<IEnumerable<Point>> pointsCollections)
        {
            var pointsArray = pointsCollections.ToArray();

            for (var i = 0; i < pointsArray.Length; i++)
            {
                var minX = pointsArray[i].Min(p => p.X);
                var minY = pointsArray[i].Min(p => p.Y);

                var replacedPoints = pointsArray[i].Select(p => new Point(p.X - minX, p.Y - minY));
                pointsArray[i] = replacedPoints;
            }

            return pointsArray;
        }

        /// <summary>
        /// Расчет дескрипторов Фурье для коллекции контуров
        /// </summary>
        /// <param name="pointsCollections">Коллекция контуров</param>
        /// <returns>Коллекция дескрипторов Фурье</returns>
        public static IEnumerable<IEnumerable<Complex>> GetFourierDescriptors(this IEnumerable<IEnumerable<Point>> pointsCollections)
        {
            var pointsArray = pointsCollections.Select(a =>  a.Select(p => new Complex(p.X, p.Y))).ToArray();

            for (var i = 0; i < pointsArray.Count(); i++)
            {
                var complexPoint = pointsArray[i].ToArray();
                FourierTransform.DFT(complexPoint, FourierTransform.Direction.Forward);

                pointsArray[i] = complexPoint;
            }

            return pointsArray;
        }
    }
}
