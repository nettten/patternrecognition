﻿namespace PatternRecognition.Core.Compare
{
    /// <summary>
    /// Результат сравнения фигуры с шаблоном
    /// </summary>
    public class PatternCompareResult
    {
        /// <summary>
        /// Процент схожести
        /// </summary>
        public double SimilarityPercent { get; }

        /// <summary>
        /// Название шаблона
        /// </summary>
        public string PatternName { get; }

        public PatternCompareResult(double similarityPercent, string patternName)
        {
            SimilarityPercent = similarityPercent;
            PatternName = patternName.Trim();
        }

        public override string ToString()
        {
            return $"{PatternName} - {SimilarityPercent}%";
        }
    }
}
