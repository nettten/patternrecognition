﻿using AForge.Math;
using PatternRecognition.Core.Storage;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PatternRecognition.Core.Compare
{
    /// <summary>
    /// Сравниватель шаблонов, представленных в виде файлов-изображений
    /// </summary>
    public class PatternComparator
    {
        /// <summary>
        /// Хранилище шаблонов
        /// </summary>
        private ShapePatternsStorage _shapeTemplatesStorage;

        public PatternComparator(ShapePatternsStorage shapeTemplatesStorage)
        {
            _shapeTemplatesStorage = shapeTemplatesStorage;
        }

        /// <summary>
        /// Сравнение дескрипторов Фурье некоторого контура с имеющимися шаблонами
        /// </summary>
        /// <param name="fourieDescriptors">Дескрипторы Фурье</param>
        /// <param name="descriptorsCount">Кол-во дескрипторов Фурье, которые будут учитываться в ходе сравнения с шаблонами</param>
        /// <param name="delta">Погрешность при сравнении</param>
        /// <returns>Результат сравнения</returns>
        public IEnumerable<PatternCompareResult> Compare(IEnumerable<Complex> fourieDescriptors, int descriptorsCount, double delta)
        {
            IList<PatternCompareResult> compareResults = new List<PatternCompareResult>();
            
            if(fourieDescriptors.Count() < descriptorsCount)
            {
                descriptorsCount = fourieDescriptors.Count();
            }

            foreach(var shapeTemplate in _shapeTemplatesStorage.Items)
            {
                var percent = 0.0d;
                var step = 100.0d / descriptorsCount;

                for (int i = 0; i < descriptorsCount; i++)
                {
                    var shapeTemplateFourierDescriptorsList = shapeTemplate.FourierDescriptors.ToList();
                    if (Math.Abs(shapeTemplateFourierDescriptorsList[i].Magnitude - fourieDescriptors.ToList()[i].Magnitude) <= delta)
                    {
                        percent += step;
                    }
                }

                compareResults.Add(new PatternCompareResult(percent, shapeTemplate.Name));
            }

            return compareResults;
        }
    }
}
