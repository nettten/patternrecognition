﻿using AForge.Math;
using System.Collections.Generic;

namespace PatternRecognition.Core.Patterns
{
    /// <summary>
    /// Шаблон фигуры
    /// </summary>
    public class ShapePattern
    {
        /// <summary>
        /// Название фигуры
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Дескрипторы Фурье
        /// </summary>
        public IEnumerable<Complex> FourierDescriptors { get; }

        public ShapePattern(string name, IEnumerable<Complex> fourierDescriptors)
        {
            Name = name.Trim();
            FourierDescriptors = fourierDescriptors;
        }
    }
}
