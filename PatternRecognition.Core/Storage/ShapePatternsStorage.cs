﻿using PatternRecognition.Core.Patterns;
using System.Collections.Generic;

namespace PatternRecognition.Core.Storage
{
    /// <summary>
    /// Хранилище дескрипторов Фурье 
    /// </summary>
    public class ShapePatternsStorage
    {
        /// <summary>
        /// Коллекция дескрипторов Фурье
        /// </summary>
        public IEnumerable<ShapePattern> Items { get; }

        public ShapePatternsStorage(IEnumerable<ShapePattern> items)
        {
            Items = items;
        }
    }
}
