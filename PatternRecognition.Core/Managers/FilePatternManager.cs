﻿using Emgu.CV;
using Emgu.CV.Structure;
using PatternRecognition.Core.Extensions;
using PatternRecognition.Core.Patterns;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace PatternRecognition.Core.Managers
{
    /// <summary>
    /// Менеджер для работы с файловыми шаблонами
    /// </summary>
    public class FilePatternManager : BasePatternManager
    {
        /// <summary>
        /// Путь к папке с шаблонами
        /// </summary>
        private readonly string _templatePath = "templates";

        /// <summary>
        /// Загрузка шаблонов из файлов
        /// </summary>
        /// <returns>Шаблоны</returns>
        public override IEnumerable<ShapePattern> Load()
        {
            var shapeTemplates = Directory.GetFiles(_templatePath)
                .Select(f =>
                {
                    using (var image = new Image<Bgr, byte>(f))
                    {
                        var fourieDescriptors = image.FindContours()
                        .ToArrayOfArray()
                        .ReplaceToCoordinateStart()
                        .GetFourierDescriptors()
                        .FirstOrDefault();

                        var fileName = Path.GetFileNameWithoutExtension(f);
                        return new ShapePattern(fileName, fourieDescriptors);
                    }

                });

            return shapeTemplates;
        }
    }
}
