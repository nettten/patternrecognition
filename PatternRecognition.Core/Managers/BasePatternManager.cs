﻿using PatternRecognition.Core.Patterns;
using System.Collections.Generic;

namespace PatternRecognition.Core.Managers
{
    /// <summary>
    /// Базовый менеджер шаблонов
    /// </summary>
    public abstract class BasePatternManager
    {
        /// <summary>
        /// Загрузка шаблонов
        /// </summary>
        /// <returns></returns>
        public abstract IEnumerable<ShapePattern> Load();
    }
}
