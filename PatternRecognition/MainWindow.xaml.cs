﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Forms;
using WPFDialogResult = System.Windows.Forms.DialogResult;
using WPFMessageBox = System.Windows.MessageBox;
using DrawingPoint = System.Drawing.Point;
using PatternRecognition.Extensions;
using PatternRecognition.Core.Extensions;
using PatternRecognition.Core.Managers;
using PatternRecognition.Core.Storage;
using PatternRecognition.Core.Compare;
using System.Windows.Threading;
using System.ComponentModel;
using System.IO;

namespace PatternRecognition
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private BasePatternManager _templateManager;
        private ShapePatternsStorage _shapeTemplatesStorage;
        private PatternComparator _patternComparator;
        private BackgroundWorker _backgroundWorker;

        private Image<Bgr, byte> _currentImage;
        private int _fourierDescriptorsCount;

        public MainWindow()
        {
            InitializeComponent();

            _templateManager = new FilePatternManager();
            _shapeTemplatesStorage
                = new ShapePatternsStorage(_templateManager.Load());
            _patternComparator = new PatternComparator(_shapeTemplatesStorage);

            _backgroundWorker = new BackgroundWorker();
            _backgroundWorker.DoWork += PatternRecognize;
            _backgroundWorker.RunWorkerCompleted += PresentRecognizeResult;
        }

        /// <summary>
        /// Открытие изображения
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OpenImage_Click(object sender, RoutedEventArgs e)
        {
            var openFileDialog = new OpenFileDialog
            {
                Filter = "Images (*.png; *.jpeg; *.jpg; *.bmp)|*.png;*.jpeg;*.jpg;*.bmp"
            };

            try
            {
                if (openFileDialog.ShowDialog() == WPFDialogResult.OK)
                {
                    if (openFileDialog.CheckFileExists)
                    {
                        using (Image<Bgr, byte> source = new Image<Bgr, byte>(openFileDialog.FileName))
                        {
                            _currentImage = new Image<Bgr, byte>(source.Bitmap);
                            sourceImage.Source = _currentImage.ToBitmapImage();
                        }

                        saveProcessedImage.IsEnabled = recognizeButton.IsEnabled = true;
                    }
                }
            }
            catch (Exception)
            {
                WPFMessageBox.Show("Ошибка при открытии изображения!");
            }
        }

        /// <summary>
        /// Выход
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Exit(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }

        private void RecognizeButton_Click(object sender, RoutedEventArgs e)
        {
            if (!_backgroundWorker.IsBusy)
            {
                BlockUI();
                progressBar.IsIndeterminate = true;
                _backgroundWorker.RunWorkerAsync();
            }
        }

        /// <summary>
        /// Распознавание образов
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PatternRecognize(object sender, DoWorkEventArgs e)
        {
            var contoursPoints = _currentImage.FindContours()
                            .ToArrayOfArray();

            var fourierDescriptors = contoursPoints
                                .ReplaceToCoordinateStart()
                                .GetFourierDescriptors();

            var compareResults = new List<IEnumerable<PatternCompareResult>>();

            foreach (var fourierDescriptor in fourierDescriptors)
            {
                compareResults.Add(_patternComparator.Compare(fourierDescriptor, _fourierDescriptorsCount, 0.0001));
            }

            for (int i = 0; i < compareResults.Count; i++)
            {
                var xC = (contoursPoints[i].Max(p => p.X) - contoursPoints[i].Min(p => p.X)) / 2 + contoursPoints[i][0].X - compareResults.ToString().Length / 2;
                var yC = (contoursPoints[i].Max(p => p.Y) - contoursPoints[i].Min(p => p.Y)) / 2 + contoursPoints[i][0].Y;

                var centroid = new DrawingPoint(xC, yC);
                var maxPercent = compareResults[i].OrderBy(r => r.SimilarityPercent).LastOrDefault();
                CvInvoke.PutText(_currentImage, maxPercent.ToString(), centroid, FontFace.HersheyDuplex, 0.5, new MCvScalar(0, 0, 0));
            }

        }

        /// <summary>
        /// Отображение результата
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PresentRecognizeResult(object sender, RunWorkerCompletedEventArgs e)
        {
            Dispatcher.Invoke(() =>
            {
                sourceImage.Source = _currentImage.ToBitmapImage();
                progressBar.IsIndeterminate = false;
                UnblockUI();
            });
        }

        /// <summary>
        /// Разблокировка UI
        /// </summary>
        private void UnblockUI()
        {
            recognizeButton.IsEnabled = true;
            menu.IsEnabled = true;
        }

        /// <summary>
        /// Блокировка UI
        /// </summary>
        private void BlockUI()
        {
            recognizeButton.IsEnabled = false;
            menu.IsEnabled = false;
        }

        /// <summary>
        /// Сохранение изображения из компонента
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SaveProcessedImage_Click(object sender, RoutedEventArgs e)
        {
            var saveFileDialog = new SaveFileDialog
            {
                Filter = "PNG|*.png|JPEG|*.jpeg|JPG|*.jpg|BMP|*.bmp"
            };

            try
            {
                if (saveFileDialog.ShowDialog() == WPFDialogResult.OK)
                {
                    _currentImage.Save(saveFileDialog.FileName);
                }

                WPFMessageBox.Show("Изображение успешно сохранено!");
            }
            catch (IOException ex)
            {
                WPFMessageBox.Show($"Произошла ошибка при сохранении изображения!{Environment.NewLine}Сообщение ошибки:{ex.Message}");
            }

        }

        private void FourierDescriptorsCountChange(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (fourierDescriptorsCountLabel != null)
            {
                fourierDescriptorsCountLabel.Content = _fourierDescriptorsCount = (int)e.NewValue;
            }
        }
    }
}
