﻿using System.Drawing;
using System.IO;
using System.Windows.Media.Imaging;

namespace PatternRecognition.Extensions
{
    /// <summary>
    /// Методы расширения для класса BitmapImage
    /// </summary>
    public static class BitmapImageExtensions
    {
        /// <summary>
        /// Преобразование изображения из представления BitmapImage в Bitmap
        /// </summary>
        /// <param name="bitmapImage">Входное изображение в представлении BitmapImage</param>
        /// <returns>Результат преобразования в виде Bitmap</returns>
        public static Bitmap ToBitmap(this BitmapImage bitmapImage)
        {
            using (var outStream = new MemoryStream())
            {
                var encoder = new BmpBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(bitmapImage));
                encoder.Save(outStream);
                return new Bitmap(outStream);
            }
        }
    }
}
