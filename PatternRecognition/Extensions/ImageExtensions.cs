﻿using Emgu.CV;
using Emgu.CV.Structure;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Media.Imaging;

namespace PatternRecognition.Extensions
{
    /// <summary>
    /// Класс, содержащий методы расширения для класса Image OpenCV
    /// </summary>
    public static class ImageExtensions
    {
        /// <summary>
        /// Преобразование из представления изображения Image OpenCV в BitmapImage
        /// </summary>
        /// <param name="source">Изображение в представлении Image OpenCV</param>
        /// <returns>Изображение в представлении BitmapImage</returns>
        public static BitmapImage ToBitmapImage(this Image<Bgr, byte> source)
        {
            BitmapImage bitmapImage = new BitmapImage();

            using (Bitmap bitmapSource = source.Bitmap)
            using (MemoryStream memory = new MemoryStream())
            {
                bitmapSource.Save(memory, ImageFormat.Png);
                memory.Position = 0;
                bitmapImage.BeginInit();
                bitmapImage.StreamSource = memory;
                bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapImage.EndInit();
            }

            return bitmapImage;
        }
    }
}
